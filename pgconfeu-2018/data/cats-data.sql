-- psql -U postgres -d kittycats < cats-data.sql

INSERT INTO cats (name, breed, origin, birthday)
  VALUES ('Kitty', 'Burmese', 'TH', '2010-12-06'),
         ('Missy', 'Chartreux', 'FR', '2014-11-11'),
         ('Sassy', 'Mau', 'EG', '2008-08-04'),
         ('Mitzi', 'Norwegian Forest Cat', 'NO', '2011-03-03');

