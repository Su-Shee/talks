-- psql -U postgres -d tapcats < traits-data.sql

INSERT INTO traits (kitty, noise, ability)
  VALUES (1, 'normal', 'nothing special'),
         (2, 'very quiet', 'looks nice'),
         (3, 'talkative', 'fetches stuff'),
         (4, 'more quiet', 'survives cold');


