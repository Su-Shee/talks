CREATE VIEW kitty_by_breed AS
  SELECT breed, origin, name
  FROM   cats;

CREATE VIEW kitty_by_country AS
  SELECT origin, breed, name
  FROM   cats;
