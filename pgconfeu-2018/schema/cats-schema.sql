-- psql -U postgres -d kittycats < cats-schema.sql

CREATE TABLE cats (
  id        SERIAL PRIMARY KEY,
  name      TEXT,
  breed     TEXT,
  origin    TEXT CHECK (LENGTH(origin) = 2),
  birthday  DATE DEFAULT current_date
);

