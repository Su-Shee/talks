#!/usr/bin/env perl

use Mojolicious::Lite;
use Data::Printer;

post '/cats' => sub {
  my $c = shift;
  app->log->debug(p($c->req->body_params));
  $c->render(json => 'foo');
};

app->start;
